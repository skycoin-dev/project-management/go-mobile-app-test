package net.skycoin.skywire.gomobile_gps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import lib.Client;
import lib.Lib;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 12345);
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.INTERNET}, 12345);
        }

        try {
            Lib.generateKeypair("12345678", getFilesDir().getAbsolutePath() + "/keys.json");
            postLocation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void postLocation() throws Exception {
        LocationManager locationManager =
                (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

        Client client = Lib.newClient("http://ap4y.dev/location");
        client.postLocation(Double.toString(location.getLatitude()),
                Double.toString(location.getLongitude()));
    }
}
