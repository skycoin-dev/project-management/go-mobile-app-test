package lib

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"fmt"
	"net/http"
	"os"
)

const (
	keyBits = 2048
	pinLen  = 8
)

type Client struct {
	gpsEndpointURL string
}

func NewClient(gpsEndpoingURL string) *Client {
	return &Client{gpsEndpoingURL}
}

func (c *Client) PostLocation(lat, lng string) error {
	body, err := json.Marshal(map[string]string{"latitude": lat, "longitude": lng})
	if err != nil {
		return fmt.Errorf("failed to marshal json: %s", err)
	}

	res, err := http.Post(c.gpsEndpointURL, "application/json", bytes.NewReader(body))
	if err != nil {
		return fmt.Errorf("http POST failed: %s", err)
	}

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("http POST failed: unexpected status code: %d", res.StatusCode)
	}

	return nil
}

func GenerateKeypair(pin, outPath string) error {
	if len(outPath) == 0 {
		return fmt.Errorf("outPath can't be empty")
	}

	privPem, pubPem, err := createKeypair(pin)
	if err != nil {
		return err
	}

	f, err := os.OpenFile(outPath, os.O_RDWR|os.O_CREATE, 0755)
	if err != nil {
		return fmt.Errorf("failed to create file: %s", err)
	}
	defer f.Close()

	err = json.NewEncoder(f).Encode(map[string]string{
		"private": string(privPem),
		"public":  string(pubPem),
	})

	if err != nil {
		return fmt.Errorf("failed to encode json: %s", err)
	}

	return nil
}

func createKeypair(pin string) ([]byte, []byte, error) {
	if len(pin) != pinLen {
		return nil, nil, fmt.Errorf("invalid pin code length. expected %d. got %d", pinLen, len(pin))
	}

	pKey, err := rsa.GenerateKey(rand.Reader, keyBits)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to generate rsa private key: %s", err)
	}

	privBlock, err := x509.EncryptPEMBlock(
		rand.Reader, "RSA PRIVATE KEY", x509.MarshalPKCS1PrivateKey(pKey), []byte(pin), x509.PEMCipherAES256,
	)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to encrypt pem block: %s", err)
	}

	pubBlock := &pem.Block{Type: "RSA PUBLIC KEY", Bytes: x509.MarshalPKCS1PublicKey(pKey.Public().(*rsa.PublicKey))}
	return pem.EncodeToMemory(privBlock), pem.EncodeToMemory(pubBlock), nil
}
