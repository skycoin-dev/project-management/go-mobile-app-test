SETUP EMULATOR TEST FOR OSX
============================

## SETUP ANDROID-SDK

### 1. Install Android Studio
[Official Guide](https://developer.android.com/studio/install#mac)

### 2. Install NDK

https://developer.android.com/ndk/guides/

### 3. Create Android Emulator 

https://developer.android.com/studio/run/managing-avds

### 4. Install Go

- Download go for Mac OS from [golang.org](https://golang.org/dl/)

- Setup environemnt variable for Go
> GOROOT: /usr/local/go

> GOPATH: /Users/UserName/go

### 5. Install gomobile and build apk

- https://github.com/golang/go/wiki/Mobile

- Init `gomobile` with ndk
```bash
$ gomobile init -ndk /Users/[user]/Library/Android/sdk/ndk-bundle
```

- Build `.apk` with `gomobile`
```bash
$ gomobile build -target=android -o=$GOPATH\src\go-mobile-app-test\SkycoinMobile.apk go-mobile-app-test
```
### 6. Install apk

- Run an AVD from the AVD Manager of Android Studio

- Run command

```bash
$ adb install SkycoinMobile.apk
```