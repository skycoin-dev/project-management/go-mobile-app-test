FROM gomobile

WORKDIR /go/src/go-mobile-app-test
COPY . .

CMD ["gomobile" "build" "-target=android" "."]
