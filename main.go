package main

import (
	"net/http"

	"golang.org/x/mobile/app"
)

func main() {
	// checkNetwork runs only once when the app first loads.
	go checkNetwork()

	app.Main(func(a app.App) {
		// Create a handler
		http.HandleFunc("/", sayHello)
		if err := http.ListenAndServe(":8080", nil); err != nil {
			panic(err)
		}
	})
}

var (
	determined = make(chan struct{})
	ok         = false
)

func checkNetwork() {
	defer close(determined)

	_, err := http.Get("http://golang.org/")
	if err != nil {
		return
	}
	ok = true
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	// Just show a simple message
	message := "Welcome to skycoin server"
	w.Write([]byte(message))
}
